import 'package:flutter/material.dart';
import 'package:no_arch_lab/my_state.dart';

class MyOtherWidget extends StatefulWidget {
  @override
  MyOtherWidgetState createState() {
    return MyOtherWidgetState();
  }
}

class MyOtherWidgetState extends State<MyOtherWidget> {
  @override Widget build(BuildContext context){
      
      return  FlatButton(
          child: Row(
            children: <Widget>[
              Icon(Icons.add),
              Text('${myState.counter}'),
            ],
          ),
          onPressed: ()=> setState(myStateController.increase,));
      
  }
}