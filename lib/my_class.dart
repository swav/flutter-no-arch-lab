import 'package:flutter/material.dart';
import 'package:no_arch_lab/my_state.dart';

class MyWidget extends StatefulWidget {
  @override
  MyWidgetState createState() {
    return MyWidgetState();
  }
}

class MyWidgetState extends State<MyWidget> {

  final MyState myRealState = MyState();
  final MyStateController myRealStateController = MyStateController();
  @override Widget build(BuildContext context){
      
      return  FlatButton(
          child: Row(
            children: <Widget>[
              Icon(Icons.add),
              Text('${myRealState.counter}'),
            ],
          ),
          onPressed: ()=> setState(myRealStateController.increase),);
      
  }
}