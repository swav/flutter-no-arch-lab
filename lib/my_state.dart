///My singleton state

//what is my singleton state
var _counter = 0;

//how do I access it
class MyState {
  int get counter => _counter;
  
}

//how do I control it
class MyStateController {
  void increase() => _counter++;
}

final MyState myState = MyState();
final MyStateController myStateController = MyStateController();

